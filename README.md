# SWP391_FE

## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

```
git init
git remote add origin https://gitlab.com/minhnqdse2003/swp391_fe.git
git fetch -a
git checkout main

Làm thì branch ra nhánh khác
git checkout -b <Tên Branch> (Vừa tạo branch mới vừa chuyển qua branch vừa mới tạo luôn)
ví dụ: Nam làm task implement Sign In Design

git checkout -b Implement/Nam/sign_In_Page (Viết theo kiểu snake case)

Lúc fetch repo về nhớ:
npm install
trước rồi mới:
npm run start
```
